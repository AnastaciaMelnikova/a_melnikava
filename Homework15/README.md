Приложение с тремя сервлетами.
1. WelcomeServlet
2. ProductListServlet
3. OrderServlet

В качестве сервера использовала Jetty.

Запуск первой стартовой страницы: http://localhost:8080/
Вторая страница: http://localhost:8080/product
Третья страница: http://localhost:8080/order