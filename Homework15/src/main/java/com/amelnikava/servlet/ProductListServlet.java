package com.amelnikava.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Second servlet.
 *
 * @author Anastasiya_Melnikava
 */
public class ProductListServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("userName");

        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<head><title>Product Servlet</title></head>");
        out.println("<body>"
                + "<h1 align=\"center\">Hello " + name + "!</h1>"
                + "<div align=\"center\">"
                + "<form method=\"post\" action=\"/order\">"
                + "<h3 align=\"center\">Make you order</h3>"
                + "<input type=\"hidden\" value=" + name + " name=\"username\">"
                + "<select name=\"products\" multiple size=\"5\">"
                + "<option disabled>Choose product(s)</option>"
                + "<option value=" + getServletContext().getInitParameter("Book") + ">Book 5.55$</option>"
                + "<option value=" + getServletContext().getInitParameter("Game") + ">Game 5.50$</option>"
                + "<option value=" + getServletContext().getInitParameter("Mobile") + ">Mobile 100.00$</option>"
                + "<option value=" + getServletContext().getInitParameter("Toy") + ">Toy 1.00$</option>"
                + "</select><br>"
                + "<input type=\"submit\" value=\"Submit\">"
                + "</form>"
                + "</div>"
                + "</body>");
        out.println("</html>");
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doGet(req, resp);
    }
}
