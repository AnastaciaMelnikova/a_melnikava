package com.amelnikava.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Third servlet.
 *
 * @author Anastasiya_Melnikava
 */
public class OrderServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("username");

        String[] order = req.getParameterValues("products");
        Map<String, Double> productMap = getProductMap(order);

        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<head><title>Order Servlet</title></head>");
        out.println("<body>"
                + "<h1 align=\"center\">Dear " + name + ", your order:</h1>"
                + "<div align=\"center\">"
                + "<p>" + printProductList(productMap) + "</p>"
                + "<h4>Total: " + getTotalSum(productMap) + "$</h4>"
                + "</div>"
                + "</body>");
        out.println("</html>");
        out.close();
    }


    /**
     * Method for creating a product list.
     *
     * @param order array of products selected by the user
     * @return list of products in the form of map
     */
    private Map<String, Double> getProductMap(String[] order) {
        Map<String, Double> productList = new HashMap<>();
        if (order != null) {
            for (String item : order) {
                String[] keyValue = item.split(":");
                productList.put(keyValue[0], Double.valueOf(keyValue[1]));
            }
        }
        return productList;
    }

    /**
     * Method for calculating the total amount.
     *
     * @param productList products selected by the user
     * @return amount
     */
    private double getTotalSum(Map<String, Double> productList) {
        return productList.values().stream().mapToDouble(Double::doubleValue).sum();
    }

    private StringBuilder printProductList(Map<String, Double> productList) {
        StringBuilder body = new StringBuilder();
        for (String item : productList.keySet()) {
            String value = productList.get(item).toString();
            body.append(item).append(" ").append(value).append("$").append("\n");
        }
        return body;
    }
}
