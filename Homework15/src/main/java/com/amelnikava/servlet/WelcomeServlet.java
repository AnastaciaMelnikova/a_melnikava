package com.amelnikava.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * First servlet.
 *
 * @author Anastasiya_Melnikava
 */
public class WelcomeServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Welcome Servlet</title></head>");
        out.println("<body>"
                + "<h1 align=\"center\">Welcome to Online Shop</h1>"
                + "<div align=\"center\">"
                + "<form action=\"/product\" method=\"get\">"
                + "<input type=\"text\" size=\"15\" name=\"userName\" value=\"Enter your name\"><br>"
                + "<button type=\"submit\">Enter</button>"
                + "</form>"
                + "</div>"
                + "</body>");
        out.println("</html>");
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doGet(req, resp);
    }
}
