package com.amelnikava;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Creation of a file system structure Test.
 *
 * @author Anastasiya_Melnikava
 */
public class PrintHierarchyTest {

    @Test
    public void buildHierarchy() {
        PrintHierarchy.print("root/folder2/folder/file1.txt");
        String[] expectedLeafs = "root/folder2/folder/file1.txt".split("/");
        int increment = 0;

        for (FileSystem child : PrintHierarchy.tree.getChildren()) {
            assertEquals(child.getParent().getName(), expectedLeafs[increment++]);
        }
    }
}
