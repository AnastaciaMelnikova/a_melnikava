package com.amelnikava;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;

/**
 * Description of the class HttpClient from task 2.
 *
 * @author Anastasiya_Melnikava
 */
public class HttpClientExample implements HttpService {

    private static final String JSON_FILE = "src/main/resources/JSON.json";

    /**
     * Method for GET request.
     *
     * @param url web service
     */
    public void sendGet(String url) {

        HttpGet request = new HttpGet(url);

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                System.out.println(EntityUtils.toString(entity));
            }
        } catch (IOException e) {
            e.getMessage();
        }
    }

    /**
     * Method for POST request.
     *
     * @param url    web service
     * @param postId user post article
     * @throws IOException
     */
    public void sendPost(String url, String postId) throws IOException {

        HttpPost post = new HttpPost(url);

        ObjectMapper objectMapper = new ObjectMapper();
        UserRequest userRequest = new UserRequest(1, Integer.parseInt(postId), "title", "message");
        objectMapper.writeValue(new File(JSON_FILE), userRequest);

        post.setEntity(new StringEntity(userRequest.toString()));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                System.out.println(EntityUtils.toString(entity));
            }
        }
        System.out.println("User request body");
        System.out.println(objectMapper.readValue(new File(JSON_FILE), UserRequest.class));
    }
}
