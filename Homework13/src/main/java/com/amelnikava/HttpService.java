package com.amelnikava;

import java.io.IOException;

/**
 * Interface for two methods.
 *
 * @author Anastasiya_Melnikava
 */
public interface HttpService {

    void sendGet(String url) throws IOException;

    void sendPost(String url, String postId) throws IOException;
}
