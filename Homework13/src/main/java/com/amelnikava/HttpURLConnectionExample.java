package com.amelnikava;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Description of the class HttpURLConnection from task 1.
 *
 * @author Anastasiya_Melnikava
 */
public class HttpURLConnectionExample implements HttpService {

    private static final String JSON_FILE = "src/main/resources/JSON.json";

    /**
     * Method for GET request.
     *
     * @param url web service
     * @throws IOException
     */
    public void sendGet(String url) throws IOException {

        HttpURLConnection httpClient = (HttpURLConnection) new URL(url).openConnection();

        httpClient.setRequestMethod("GET");
        httpClient.setRequestProperty("Content-Type", "text/html");

        printResult(httpClient);
    }

    /**
     * Method for POST request.
     *
     * @param url    web service
     * @param postId user post article
     * @throws Exception
     */
    public void sendPost(String url, String postId) throws IOException {

        HttpURLConnection httpClient = (HttpURLConnection) new URL(url).openConnection();

        httpClient.setRequestMethod("POST");
        httpClient.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpClient.setRequestProperty("charset", "utf-8");


        httpClient.setDoOutput(true);
        httpClient.setDoInput(true);

        String json = "{ \"userId\": 1,\n" +
                "  \"id\": 102,\n" +
                "  \"title\": \"title\",\n" +
                "  \"body\": \"message\"}";

        try (DataOutputStream wr = new DataOutputStream(httpClient.getOutputStream())) {
            wr.writeBytes(json);
            wr.flush();
        }
        printResult(httpClient);
    }

    /**
     * Method for print result request.
     *
     * @param httpClient server connection
     */
    private void printResult(HttpURLConnection httpClient) {
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(httpClient.getInputStream()))) {

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            System.out.println(response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
