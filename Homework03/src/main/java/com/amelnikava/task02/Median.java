package com.amelnikava.task02;

import java.util.Arrays;

/**
 * Task 2 Class is described finding the median in the array.
 *
 * @author Anastasiya_Melnikava
 */
public final class Median {

    private Median() {
        throw new AssertionError();
    }

    /**
     * Method to display card balance.
     *
     * @param intsNumbers array of ints is passed to the method
     * @return the median of the array.
     */
    public static float median(int[] intsNumbers) {
        Arrays.sort(intsNumbers);
        int middle = intsNumbers.length / 2;
        float medianValue;
        if (intsNumbers.length % 2 == 1) {
            medianValue = intsNumbers[middle];
        } else {
            medianValue = (float) (intsNumbers[middle - 1] + intsNumbers[middle]) / 2;
        }
        return medianValue;
    }

    /**
     * Method to display card balance.
     *
     * @param doublesNumbers array of doubles is passed to the method
     * @return the median of the array.
     */
    public static double median(double[] doublesNumbers) {
        Arrays.sort(doublesNumbers);
        int middle = doublesNumbers.length / 2;
        double medianValue;
        if (doublesNumbers.length % 2 == 1) {
            medianValue = doublesNumbers[middle];
        } else {
            medianValue = (doublesNumbers[middle - 1] + doublesNumbers[middle]) / 2;
        }
        return medianValue;
    }
}
