package com.amelnikava.task01;

import java.math.BigDecimal;

/**
 * Task 1 Class is described card.
 *
 * @author Anastasiya_Melnikava
 */
public class Card {

    private String cardholderName;
    private BigDecimal accountBalance;

    public Card(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public Card(String cardholderName, BigDecimal accountBalance) {
        this.cardholderName = cardholderName;
        this.accountBalance = accountBalance;
    }

    /**
     * Method to display card balance.
     *
     * @return the balance value in double.
     */
    public double getCardBalance() {
        BigDecimal cardBalance = accountBalance.setScale(2, BigDecimal.ROUND_HALF_UP);
        return cardBalance.doubleValue();
    }

    /**
     * Method for adding amount to balance.
     *
     * @param addition number to add
     */
    public void addToCardBalance(BigDecimal addition) {
        accountBalance = accountBalance.add(addition);
    }

    /**
     * Method for withdraw amount from balance.
     *
     * @param withdraw number to subtract
     */
    public void withdrawFromCardBalance(BigDecimal withdraw) {
        accountBalance = accountBalance.subtract(withdraw);
    }

    /**
     * Method for displaying the balance in the currency.
     *
     * @param exchangeRate required exchange rate
     * @return the balance value in double.
     */
    public double getCardBalanceDifferentCurrency(double exchangeRate) {
        BigDecimal cardBalanceInCurrency = accountBalance
                .divide(BigDecimal.valueOf(exchangeRate), 2, BigDecimal.ROUND_HALF_UP);
        return cardBalanceInCurrency.doubleValue();
    }
}
