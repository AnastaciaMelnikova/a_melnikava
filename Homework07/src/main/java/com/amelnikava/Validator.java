package com.amelnikava;

/**
 * Abstract validator class.
 *
 * @author Anastasiya_Melnikava
 */
public abstract class Validator<T> {

    private T object;

    public Validator(T object) {
        this.object = object;
    }

    public abstract boolean isValid();
}
