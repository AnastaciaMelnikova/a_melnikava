package com.amelnikava;

/**
 * String validator class.
 *
 * @author Anastasiya_Melnikava
 */
public class StringValidator extends Validator {

    private final String REGEX = "^[A-Z].*$";

    private String string;

    public StringValidator(Object object) {
        super(object);
        this.string = object.toString();
    }

    /**
     * Method for validation a string with an upper letter.
     *
     * @return true or false.
     */
    @Override
    public boolean isValid() {
        return this.string.matches(REGEX);
    }
}
