package com.amelnikava;

/**
 * Factory class.
 *
 * @author Anastasiya_Melnikava
 */
public class ValidationSystem {

    private static Validator validator;

    public static Validator validate(Object object) throws ValidationFailedException {
        if (object instanceof Integer) {
            validator = new IntegerValidator(object);
        } else if (object instanceof String) {
            validator = new StringValidator(object);
        } else {
            throw new ValidationFailedException("Validation failed");
        }
        if (validator.isValid()) {
            return validator;
        } else {
            throw new ValidationFailedException("Validation failed");
        }
    }
}
