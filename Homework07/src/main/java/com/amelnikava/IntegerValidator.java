package com.amelnikava;

/**
 * Integer validator class.
 *
 * @author Anastasiya_Melnikava
 */
public class IntegerValidator extends Validator {

    private final int LOWER_BOUND = 1;
    private final int UPPER_BOUND = 10;

    private Integer number;

    public IntegerValidator(Object object) {
        super(object);
        this.number = new Integer(object.toString());
    }

    /**
     * Method for validation in the range of values.
     *
     * @return true or false.
     */
    @Override
    public boolean isValid() {
        return number <= UPPER_BOUND && number >= LOWER_BOUND;
    }
}
