package com.amelnikava;

import javax.xml.bind.ValidationException;

/**
 * Class is described exception validation.
 *
 * @author Anastasiya_Melnikava
 */
public class ValidationFailedException extends ValidationException {

    public ValidationFailedException(String message) {
        super(message);
    }
}
