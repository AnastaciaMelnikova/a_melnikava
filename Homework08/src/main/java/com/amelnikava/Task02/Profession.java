package com.amelnikava.Task02;

/**
 * Class enum contains professions.
 *
 * @author Anastasiya_Melnikava
 */
public enum Profession {

    FOREMAN,
    MECHANIC,
    WELDER,
    ENGINEER,
    WORKER,
    PLASTERER,
    ELECTRICIAN
}
