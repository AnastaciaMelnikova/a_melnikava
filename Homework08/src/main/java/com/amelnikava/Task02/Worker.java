package com.amelnikava.Task02;

/**
 * Class describes worker with profession and his salary.
 *
 * @author Anastasiya_Melnikava
 */
public class Worker {

    private Profession profession;
    private double salary;

    public Worker(Profession profession, double salary) {
        this.profession = profession;
        this.salary = salary;
    }

    public Profession getProfessions() {
        return profession;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Worker: " +
                "profession = " + profession +
                ", salary = " + salary;
    }
}
