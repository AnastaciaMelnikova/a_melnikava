package com.amelnikava.Task02;

import java.util.Set;

/**
 * Class describes brigades which consist of workers.
 *
 * @author Anastasiya_Melnikava
 */
public class Brigade {

    private Set<Worker> employees;

    public Brigade(Set<Worker> employees) {
        this.employees = employees;
    }

    public Set<Worker> getEmployees() {
        return employees;
    }

    @Override
    public String toString() {
        return "Brigade: " +
                "employees = " + employees;
    }
}
