package com.amelnikava.Task02;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Class describes construction tender.
 *
 * @author Anastasiya_Melnikava
 */
public class TenderService {

    private Set<Brigade> brigades;
    private Double estimate;
    private List<Profession> professionList;

    public TenderService(Set<Brigade> brigades, Double estimate, List<Profession> professionList) {
        this.brigades = brigades;
        this.estimate = estimate;
        this.professionList = professionList;
    }

    public List<Brigade> startTender() {
        return brigades.stream()
                .filter(getProfessionalFilter())
                .filter(getSalaryFilter())
                .collect(Collectors.toList());
    }

    private Predicate<? super Brigade> getSalaryFilter() {
        return brigade -> brigade.getEmployees().stream().mapToDouble(Worker::getSalary).sum() <= estimate;
    }

    private Predicate<? super Brigade> getProfessionalFilter() {
        return brigade -> brigade.getEmployees().stream()
                .map(Worker::getProfessions)
                .collect(Collectors.toList()).containsAll(professionList);
    }

    public void printResult(List<Brigade> workers) {
        if (!workers.isEmpty()) {
            workers.forEach(System.out::println);
        } else {
            System.out.println("Tender for construction is closed. The brigade was not found.");
        }
    }
}
