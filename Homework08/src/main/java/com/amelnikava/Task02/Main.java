package com.amelnikava.Task02;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The class is written for testing.
 *
 * @author Anastasiya_Melnikava
 */
public class Main {

    private static final double ESTIMATE = 3000.00;

    public static void main(String[] args) {

        Worker worker1 = new Worker(Profession.ENGINEER, 1300.00);
        Worker worker2 = new Worker(Profession.ELECTRICIAN, 800.00);
        Worker worker3 = new Worker(Profession.FOREMAN, 1500.00);
        Worker worker4 = new Worker(Profession.MECHANIC, 800.00);
        Worker worker5 = new Worker(Profession.PLASTERER, 900.00);
        Worker worker6 = new Worker(Profession.WELDER, 950.00);
        Worker worker7 = new Worker(Profession.WORKER, 600.00);

        Set<Worker> workerSet1 = new HashSet<>();
        workerSet1.add(worker1);
        workerSet1.add(worker2);
        workerSet1.add(worker4);

        Set<Worker> workerSet2 = new HashSet<>();
        workerSet2.add(worker3);
        workerSet2.add(worker2);
        workerSet2.add(worker5);
        workerSet2.add(worker7);
        workerSet2.add(worker1);

        Set<Worker> workerSet3 = new HashSet<>();
        workerSet3.add(worker3);
        workerSet3.add(worker6);
        workerSet3.add(worker5);
        workerSet3.add(worker7);

        Brigade brigade1 = new Brigade(workerSet1);
        Brigade brigade2 = new Brigade(workerSet2);
        Brigade brigade3 = new Brigade(workerSet3);

        Set<Brigade> brigades = new HashSet<>();
        brigades.add(brigade1);
        brigades.add(brigade2);
        brigades.add(brigade3);

        List<Profession> professionList = new ArrayList<>();
        professionList.add(Profession.ELECTRICIAN);
        professionList.add(Profession.ENGINEER);
        professionList.add(Profession.MECHANIC);

        TenderService tender = new TenderService(brigades, ESTIMATE, professionList);
        tender.printResult(tender.startTender());
    }
}
