package com.amelnikava.Task01;

import java.util.ArrayList;

/**
 * Class describes the implementation of user arrays list.
 *
 * @param <E> the type of elements in this list
 * @author Anastasiya_Melnikava
 */
public class UserArrayList<E> extends ArrayList<E> {

    /**
     * Limited default initial capacity.
     */
    private static final int DEFAULT_CAPACITY = 10;
    private static final int DEFAULT_SIZE = 0;
    private static final String SIZE_FULL_EXCEPTION = "The list is limited in size and it is filled. " +
            "Element didn't add";
    private Object[] data;
    private int size;
    private int capacity;

    /**
     * Creating a new array list with the specified initial capacity.
     *
     * @param capacity size of the array
     * @throws IllegalArgumentException if the specified initial capacity
     *                                  is negative
     */
    public UserArrayList(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Capacity cannot be less than 0.");
        } else {
            this.capacity = capacity;
            this.size = DEFAULT_SIZE;
            this.data = new Object[this.capacity];
        }
    }

    /**
     * Creating a new array without parameters.
     */
    public UserArrayList() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Get the size of the array.
     *
     * @return the size of the array
     */
    public int getSize() {
        return this.size;
    }

    /**
     * Output array to the screen.
     *
     * @return array list in string form
     */
    @Override
    public String toString() {
        StringBuilder arr = new StringBuilder();
        for (int i = 0; i < size; i++) {
            arr.append(data[i].toString()).append(" ");
        }
        return arr.toString();
    }

    private void ensureCapacity() throws ListSizeIsFullException {
        if (this.size == this.capacity) {
            throw new ListSizeIsFullException(SIZE_FULL_EXCEPTION);
        }
    }

    /**
     * Add a new element to the end of this list.
     *
     * @param value element to be appended to this list
     * @return true or false
     */
    @Override
    public boolean add(E value) {
        try {
            ensureCapacity();
            data[size++] = value;
        } catch (ListSizeIsFullException e) {
            System.out.println(e.getMessage());
        }
        return true;
    }

    /**
     * Insert a new item in this list at the specified index.
     *
     * @param index index at which the specified element is to be inserted
     * @param value element to be inserted
     * @throws IndexOutOfBoundsException to indicate that an index is out of range
     */
    @Override
    public void add(int index, E value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Illegal index");
        }
        try {
            ensureCapacity();
        } catch (ListSizeIsFullException e) {
            System.out.println(e.getMessage());
        }
        for (int i = size; i > index; i--) {
            data[i] = data[i - 1];
        }
        data[index] = value;
        size++;
    }

    /**
     * Delete one item at the specified index in this list.
     *
     * @param index the index of the element to be removed
     */
    private void removeAt(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Illegal index");
        }
        for (int i = index + 1; i < size; i++) {
            data[i - 1] = data[i];
        }
        size--;
    }

    /**
     * Delete one item whose value matches the value of the passed parameter.
     *
     * @param value element to be removed from this list
     * @return true if this list contained the specified element
     */
    @Override
    public boolean remove(Object value) {
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (data[i].equals(value)) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            removeAt(index);
            return true;
        }
        return false;
    }

    /**
     * Removes all of the elements from this list.
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            data[i] = null;
        }
        this.size = DEFAULT_SIZE;
    }

    /**
     * Check on empty array list.
     *
     * @return true if the list is empty
     */
    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    /**
     * Method fits the value of capacity to size
     */
    @Override
    public void trimToSize() {
        this.capacity = this.size;
        try {
            ensureCapacity();
        } catch (ListSizeIsFullException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Search first specified value.
     *
     * @param value - value of the passed parameter
     * @return a number of index
     */
    @Override
    public int indexOf(Object value) {
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (data[i].equals(value)) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * Search for the last value specified.
     *
     * @param value - value of the passed parameter
     * @return a number of index
     */
    @Override
    public int lastIndexOf(Object value) {
        int index = -1;
        for (int i = size - 1; i >= 0; i--) {
            if (data[i].equals(value)) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * Returns the element at the specified index.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException to indicate that an index is out of range
     */
    @Override
    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Illegal index");
        }
        return (E) data[index];
    }
}
