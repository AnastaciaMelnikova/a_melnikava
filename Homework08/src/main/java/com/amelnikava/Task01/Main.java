package com.amelnikava.Task01;

import java.util.Scanner;

/**
 * The class is written for testing in the console.
 *
 * @author Anastasiya_Melnikava
 */
public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Creating the array list");
        UserArrayList arrayList = new UserArrayList();
        int choice;
        do {
            System.out.println("Make a choice");
            System.out.println("1. The size of the list");
            System.out.println("2. Output array to screen");
            System.out.println("3. Add a new element to the list");
            System.out.println("4. Insert a new item into list at the specified index");
            System.out.println("5. Delete one item whose value matches the value of the passed parameter");
            System.out.println("6. Clear list");
            System.out.println("7. Check on empty array");
            System.out.println("8. Search first specified value");
            System.out.println("9. Search for the last value specified");
            System.out.println("10. Get element at specified index");
            System.out.println("11. Exit\n");
            System.out.print("Your choice: ");

            do {
                while (!scanner.hasNextInt()) {
                    System.out.println("Choose one");
                    scanner.nextLine();
                }
                choice = scanner.nextInt();
            } while (choice < 1 || choice > 11);

            switch (choice) {
                case 1:
                    System.out.println(arrayList.getSize());
                    break;
                case 2:
                    System.out.println(arrayList.toString());
                    System.out.println();
                    break;
                case 3:
                    System.out.println("Enter the item");
                    Object itemBack = enterValue();
                    arrayList.add(itemBack);
                    break;
                case 4:
                    System.out.println("Enter the index");
                    int index = enterIntValue();
                    System.out.println("Enter the item");
                    Object insert = enterValue();
                    arrayList.add(index, insert);
                    break;
                case 5:
                    System.out.println("Enter the value");
                    Object value = enterValue();
                    arrayList.remove(value);
                    break;
                case 6:
                    arrayList.clear();
                    break;
                case 7:
                    System.out.println(arrayList.isEmpty());
                    break;
                case 8:
                    System.out.println("Enter the value");
                    Object valueIndexOf = enterValue();
                    System.out.println(arrayList.indexOf(valueIndexOf));
                    break;
                case 9:
                    System.out.println("Enter the value");
                    Object valueLastIndexOf = enterValue();
                    System.out.println(arrayList.lastIndexOf(valueLastIndexOf));
                    break;
                case 10:
                    System.out.println("Enter the index");
                    int indexGetElement = enterIntValue();
                    System.out.println(arrayList.get(indexGetElement));
                    break;
                default:
                    break;
            }
        } while (choice != 11);
        scanner.close();
    }

    private static Object enterValue() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    private static int enterIntValue() {
        Scanner scanner = new Scanner(System.in);
        int index;
        do {
            while (!scanner.hasNextInt()) {
                scanner.nextLine();
            }
            index = scanner.nextInt();
        } while (index < 0);
        return index;
    }
}
