package com.amelnikava.Task01;

/**
 * Class describes exception.
 *
 * @author Anastasiya_Melnikava
 */
public class ListSizeIsFullException extends Exception {

    public ListSizeIsFullException(String message) {
        super(message);
    }
}
