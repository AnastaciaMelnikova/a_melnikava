package com.amelnikava.Task01;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserArrayListTest {

    private UserArrayList<Integer> testArrayList;

    @Before
    public void setUp() {
        testArrayList = new UserArrayList<>();
        testArrayList.add(10);
        testArrayList.add(15);
    }

    @Test
    public void testIndexOf() {
        int expected = 1;
        Assert.assertEquals(expected, testArrayList.indexOf(15));
    }

    @Test
    public void testGet() {
        Integer expected = 15;
        Assert.assertEquals(expected, testArrayList.get(1));
    }

    @Test
    public void testClear() {
        testArrayList.clear();
        int sizeExpected = 0;
        Assert.assertEquals(sizeExpected, testArrayList.getSize());
    }

    @Test
    public void testIsEmpty() {
        Assert.assertFalse(testArrayList.isEmpty());
    }

    @Test
    public void testIsEmptyAfterClear() {
        testArrayList.clear();
        Assert.assertTrue(testArrayList.isEmpty());
    }
}
