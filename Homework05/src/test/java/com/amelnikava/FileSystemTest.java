package com.amelnikava;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

/**
 * File system class Test.
 *
 * @author Anastasiya_Melnikava
 */
public class FileSystemTest {

    @Test
    public void print() throws Exception {
        PrintHierarchy.print("root/folder2/folder");
        String expected = "root\r\n--folder2\r\n----folder\r\n";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(outputStream);
        System.setOut(ps);

        PrintHierarchy.tree.print(1);

        assertEquals(expected, outputStream.toString());
    }
}
