package com.amelnikava;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Application main class.
 *
 * @author Anastasiya_Melnikava
 */
public class Main {

    private static final int ONE = 1;
    private static final int THREE = 3;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Write the path:");
            String path = scanner.nextLine();

            System.out.println("\nChoose:");
            System.out.println("1. Print the path");
            System.out.println("2. Add the path");
            System.out.println("3. Exit");
            int choose = getChoose(scanner);

            if (choose == 1) {
                PrintHierarchy.print(path);
                PrintHierarchy.tree.print(1);
            } else if (choose == 2) {
                path = scanner.nextLine();
            } else {
                scanner.close();
                return;
            }
        }
    }

    /**
     * Method for get correct choose from user.
     *
     * @param scanner object for receiving data from the user.
     * @return correct choose from user
     */
    private static int getChoose(Scanner scanner) {
        int selectedNumber;
        do {
            try {
                selectedNumber = scanner.nextInt();
                if (selectedNumber < ONE || selectedNumber > THREE) {
                    System.out.println("You have not entered a need number. Try again.");
                } else {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("You have entered an invalid value. Try again.");
                scanner.nextLine();
            }
        } while (true);
        return selectedNumber;
    }
}
