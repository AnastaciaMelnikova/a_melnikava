package com.amelnikava;

import java.util.ArrayList;
import java.util.List;

/**
 * Class is described file system hierarchy.
 *
 * @author Anastasiya_Melnikava
 */
public class FileSystem {

    private String name;
    private FileSystem parent;
    private List<FileSystem> children = new ArrayList<>();

    public FileSystem() {

    }

    public FileSystem(String name, FileSystem parent) {
        this.name = name;
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public FileSystem getParent() {
        return parent;
    }

    public List<FileSystem> getChildren() {
        return children;
    }

    public void print(int level) {
        for (int i = 1; i < level; i++) {
            System.out.print("--");
        }
        System.out.println(name);
        for (FileSystem child : children) {
            child.print(level + 1);
        }
    }
}
