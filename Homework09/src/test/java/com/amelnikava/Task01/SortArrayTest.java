package com.amelnikava.Task01;

import org.junit.Assert;
import org.junit.Test;

/**
 * The class for testing basic methods.
 *
 * @author Anastasiya_Melnikava
 */
public class SortArrayTest {

    @Test
    public void testSum() {
        int actual = SortArray.sum(56);
        int expected = 11;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testSortArray() {
        Integer[] actual = {15, 20, 56, 12};
        Integer[] expected = {20, 12, 15, 56};
        SortArray.sortArray(actual);
        Assert.assertArrayEquals(expected, actual);
    }
}