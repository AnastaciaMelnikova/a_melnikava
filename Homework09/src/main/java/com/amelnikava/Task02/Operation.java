package com.amelnikava.Task02;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The main class for operations on sets.
 * The class without tests, because there are no methods.
 *
 * @author Anastasiya_Melnikava
 */
public class Operation {

    public static void main(String[] args) {

        List<String> listA = Arrays.asList("A", "B", "C", "D", "E", "F", "G");
        List<String> listB = Arrays.asList("E", "F", "G", "A", "H", "O");

        Set<String> union = Stream.concat(listA.stream(), listB.stream())
                .collect(Collectors.toSet());

        Set<String> intersection = listA.stream()
                .filter(listB::contains)
                .collect(Collectors.toSet());

        Set<String> difference = listA.stream()
                .filter(i -> !listB.contains(i))
                .collect(Collectors.toSet());

        Set<String> symmetricDifference = new HashSet<>(listA);
        symmetricDifference.addAll(listB);
        Set<String> temp = new HashSet<>(listA);
        temp.retainAll(listB);
        symmetricDifference.removeAll(temp);

        System.out.println("Union operation: " + union);
        System.out.println("Intersection operation: " + intersection);
        System.out.println("Minus operation: " + difference);
        System.out.println("Difference operation: " + symmetricDifference);
    }
}
