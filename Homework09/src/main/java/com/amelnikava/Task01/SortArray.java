package com.amelnikava.Task01;

import java.util.Arrays;
import java.util.Random;

/**
 * The main class is written for sorting an array by the sum of its digits.
 *
 * @author Anastasiya_Melnikava
 */
public class SortArray {

    private static final int SIZE = 10;
    private static final int BORDER = 100;

    public static void main(String[] args) {

        Integer[] array = new Integer[SIZE];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(BORDER) + 1;
        }

        System.out.println("Original array: " + Arrays.toString(array));
        sortArray(array);
        System.out.println("Sort result: " + Arrays.toString(array));
    }

    /**
     * Method for sorting an array.
     *
     * @param array is the array to be sorted
     */
    static void sortArray(Integer[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (sum(array[j]) > sum(array[j + 1])) {
                    Integer temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    /**
     * Method for counting the sum of digits of a number.
     *
     * @param number is the number in array
     */
    static int sum(int number) {
        int sum = 0;
        while (number != 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }
}
