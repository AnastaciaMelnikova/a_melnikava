package com.amelnikava;

/**
 * Class is describes the creation of a file system structure.
 *
 * @author Anastasiya_Melnikava
 */
public class PrintHierarchy {

    public static FileSystem tree;

    public static void print(String path) {
        String[] leafs = path.split("/");
        FileSystem parent = null;
        for (String leaf : leafs) {
            if (parent == null) {
                if (tree == null) {
                    tree = new FileSystem(leaf, null);
                }
                parent = tree;
            } else {
                FileSystem found = null;
                for (FileSystem child : parent.getChildren()) {
                    if (child.getName().equals(leaf)) {
                        found = child;
                        break;
                    }
                }
                if (found == null) {
                    found = new FileSystem(leaf, parent);
                    parent.getChildren().add(found);
                }
                parent = found;
                if (leaf.contains(".")) {
                    break;
                }
            }
        }
    }
}
