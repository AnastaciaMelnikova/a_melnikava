package com.amelnikava;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Application main class.
 *
 * @author Anastasiya_Melnikava
 */
public class Main {

    private static final int ONE = 1;
    private static final int FIVE = 5;
    private static final String NAME_FILE = "file.txt";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Write the path:");
            String path = scanner.nextLine();

            System.out.println("\nChoose:");
            System.out.println("1. Print the path");
            System.out.println("2. Add the path");
            System.out.println("3. Save in file");
            System.out.println("4. Load file");
            System.out.println("5. Exit");
            int choose = getChoose(scanner);

            if (choose == 1) {
                PrintHierarchy.print(path);
                PrintHierarchy.tree.print(1);
            } else if (choose == 2) {
                System.out.println("Write the path:");
                scanner.nextLine();
            } else if (choose == 3) {
                saveFile(PrintHierarchy.tree);
            } else if (choose == 4) {
                PrintHierarchy.tree = loadFile();
            } else {
                scanner.close();
                return;
            }
        }
    }

    /**
     * Method for uploading a file.
     *
     * @return the downloaded file as an object FileSystem
     */
    private static FileSystem loadFile() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(NAME_FILE))) {
            return (FileSystem) ois.readObject();
        } catch (ClassNotFoundException | IOException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    /**
     * Method for saving structure hierarchy to file.
     *
     * @param tree object with recorded paths from the user.
     */
    private static void saveFile(FileSystem tree) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(NAME_FILE))) {
            oos.writeObject(tree);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Method for get correct choose from user.
     *
     * @param scanner object for receiving data from the user.
     * @return correct choose from user
     */
    private static int getChoose(Scanner scanner) {
        int selectedNumber;
        do {
            try {
                selectedNumber = scanner.nextInt();
                if (selectedNumber < ONE || selectedNumber > FIVE) {
                    System.out.println("You have not entered a need number. Try again.");
                } else {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("You have entered an invalid value. Try again.");
                scanner.nextLine();
            }
        } while (true);
        return selectedNumber;
    }
}
