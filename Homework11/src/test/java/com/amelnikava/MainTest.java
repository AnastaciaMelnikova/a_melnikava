package com.amelnikava;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class MainTest {

    @Test
    public void testSaveFileNotNull() throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("test.txt"));
        oos.writeObject("root/folder2");
        Assert.assertNotNull(oos);
    }

    @Test
    public void testLoadFile() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("test.txt"));
        String expected = "root/folder2";
        Assert.assertEquals(expected, ois.readObject());
    }
}
