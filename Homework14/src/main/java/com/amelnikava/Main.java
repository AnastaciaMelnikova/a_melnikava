package com.amelnikava;

import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Main application class.
 *
 * @author Anastasiya_Melnikava
 */
public class Main {

    private static final String URL = "http://jsonplaceholder.typicode.com/posts/";
    private static final int COUNT_ARGUMENTS = 3;
    private static final Logger LOGGER = Logger.getLogger(Main.class);
    private static final String ERROR = "Sorry, something wrong!";

    /**
     * Application start point.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {

        /*
          The application accepts three parameters as input.
          The first of them is the connection class: URL or CLIENT.
          The second of them is the request method: GET or POST.
          The third is custom value id article.
         */
        if (args == null || args.length < COUNT_ARGUMENTS) {
            LOGGER.error("Not enough arguments");
            return;
        }

        HttpService connection = null;
        if (args[0].equals("URL")) {
            connection = new HttpURLConnectionExample();
        } else if (args[0].equals("CLIENT")) {
            connection = new HttpClientExample();
        }

        String postId = args[2];

        if (connection != null) {
            if (args[1].equals("GET")) {
                try {
                    LOGGER.info(connection.sendGet(URL + postId));
                } catch (IOException e) {
                    LOGGER.error(ERROR, e);
                }
            } else if (args[1].equals("POST")) {
                try {
                    LOGGER.info(connection.sendPost(URL, postId));
                } catch (IOException e) {
                    LOGGER.error(ERROR, e);
                }
            }
        }
    }
}
