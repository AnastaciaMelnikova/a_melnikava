package com.amelnikava;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Description of the class HttpClient from task 2.
 *
 * @author Anastasiya_Melnikava
 */
public class HttpClientExample implements HttpService {

    private static final String JSON_FILE = "src/main/resources/JSON.json";
    private static final Logger LOGGER = Logger.getLogger(HttpClientExample.class);

    /**
     * Method for GET request.
     *
     * @param url web service
     * @return response from web server
     */
    public String sendGet(String url) {

        HttpGet request = new HttpGet(url);

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity);
            }
        } catch (IOException e) {
            LOGGER.error("Sorry, something wrong!", e);
        }
        return null;
    }

    /**
     * Method for POST request.
     *
     * @param url    web service
     * @param postId user post article
     * @return response from web server
     */
    public String sendPost(String url, String postId) throws IOException {

        HttpPost post = new HttpPost(url);

        ObjectMapper objectMapper = new ObjectMapper();
        UserRequest userRequest = new UserRequest(1, Integer.parseInt(postId), "title", "message");
        objectMapper.writeValue(new File(JSON_FILE), userRequest);

        post.setEntity(new StringEntity(userRequest.toString()));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                System.out.println("User request body");
                LOGGER.info(objectMapper.readValue(new File(JSON_FILE), UserRequest.class));
                return EntityUtils.toString(entity);
            }
        }
        return null;
    }
}
