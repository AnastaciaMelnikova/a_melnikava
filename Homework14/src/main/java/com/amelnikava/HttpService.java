package com.amelnikava;

import java.io.IOException;

/**
 * Interface for two methods.
 *
 * @author Anastasiya_Melnikava
 */
public interface HttpService {

    String sendGet(String url) throws IOException;

    String sendPost(String url, String postId) throws IOException;
}
