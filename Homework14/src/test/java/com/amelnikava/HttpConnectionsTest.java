package com.amelnikava;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Optional;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class HttpConnectionsTest {

    @Mock
    private HttpURLConnectionExample httpURLConnectionExample;
    @Mock
    private HttpClientExample httpClientExample;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHttpUrlConnectionsSendPost() throws IOException {

//      given:
        String url = "http://jsonplaceholder.typicode.com/posts/";
        String response = "{\n" +
                "userId: 1\n" +
                "id: 102\n" +
                "title: some title\n" +
                "body: some message\n" +
                "}";
        UserRequest expected = new UserRequest(1, 102, "some title", "some message");

        when(httpURLConnectionExample.sendPost(eq(url), anyString())).thenReturn(response);

//      when:
        Optional<String> actual = Optional.ofNullable(httpURLConnectionExample.sendPost(url, "102"));

//      then:
        Assert.assertTrue(actual.isPresent());
        Assert.assertEquals(expected.toString(), actual.get());

        verify(httpURLConnectionExample).sendPost(eq(url), anyString());
    }

    @Test
    public void testHttpUrlConnectionsSendGet() throws IOException {

//      given:
        String url = "http://jsonplaceholder.typicode.com/posts/";
        String response = "{\n" +
                "userId: 1\n" +
                "id: 1\n" +
                "title: some title\n" +
                "body: some message\n" +
                "}";
        UserRequest expected = new UserRequest(1, 1, "some title", "some message");

        when(httpURLConnectionExample.sendGet(eq(url))).thenReturn(response);

//      when:
        Optional<String> actual = Optional.ofNullable(httpURLConnectionExample.sendGet(url));

//      then:
        Assert.assertTrue(actual.isPresent());
        Assert.assertEquals(expected.toString(), actual.get());

        verify(httpURLConnectionExample).sendGet(eq(url));
    }

    @Test
    public void testHttpClientSendPost() throws IOException {

//      given:
        String url = "http://jsonplaceholder.typicode.com/posts/";
        String response = "{\n" +
                "userId: 1\n" +
                "id: 102\n" +
                "title: some title\n" +
                "body: some message\n" +
                "}";
        UserRequest expected = new UserRequest(1, 102, "some title", "some message");

        when(httpClientExample.sendPost(eq(url), anyString())).thenReturn(response);

//      when:
        Optional<String> actual = Optional.ofNullable(httpClientExample.sendPost(url, "102"));

//      then:
        Assert.assertTrue(actual.isPresent());
        Assert.assertEquals(expected.toString(), actual.get());

        verify(httpClientExample).sendPost(eq(url), anyString());
    }

    @Test
    public void testHttpClientSendGet() {

//      given:
        String url = "http://jsonplaceholder.typicode.com/posts/";
        String response = "{\n" +
                "userId: 1\n" +
                "id: 102\n" +
                "title: some title\n" +
                "body: some message\n" +
                "}";
        UserRequest expected = new UserRequest(1, 102, "some title", "some message");

        when(httpClientExample.sendGet(eq(url))).thenReturn(response);

//      when:
        Optional<String> actual = Optional.ofNullable(httpClientExample.sendGet(url));

//      then:
        Assert.assertTrue(actual.isPresent());
        Assert.assertEquals(expected.toString(), actual.get());

        verify(httpClientExample).sendGet(eq(url));
    }

    @Test(expected = IOException.class)
    public void testArticleServiceGetWithIOException() {

//      given:
        String url = "http://jsonplaceholder.typicode.com/posts/";

        when(httpClientExample.sendGet(eq(url))).thenThrow(IOException.class);

//      when:
        Optional<String> actual = Optional.ofNullable(httpClientExample.sendGet(url));

//      then:
        verify(httpClientExample).sendGet(eq(url));
    }
}
