package com.amelnikava.task02;

import org.junit.Assert;
import org.junit.Test;

/**
 * Sorting Context Test
 *
 * @author Anastasiya_Melnikava
 */
public class SortingContextTest {

    private SortStrategy sortStrategy;
    private SortingContext context;
    private final int[] TEST_ARRAY = new int[]{51, 55, 15, 10, 100, 8, 5, 0};

    @Test
    public void testBubbleSort() {
        sortStrategy = new BubbleSort();
        context = new SortingContext(sortStrategy);
        context.execute(TEST_ARRAY);
        int[] expectedArray = new int[]{0, 5, 8, 10, 15, 51, 55, 100};
        Assert.assertArrayEquals(expectedArray, TEST_ARRAY);
    }

    @Test
    public void testSelectionSort() {
        sortStrategy = new SelectionSort();
        context = new SortingContext(sortStrategy);
        context.execute(TEST_ARRAY);
        int[] expectedArray = new int[]{0, 5, 8, 10, 15, 51, 55, 100};
        Assert.assertArrayEquals(expectedArray, TEST_ARRAY);
    }
}
