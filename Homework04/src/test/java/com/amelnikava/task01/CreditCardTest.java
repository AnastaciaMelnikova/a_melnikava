package com.amelnikava.task01;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Class Credit Card Test
 *
 * @author Anastasiya_Melnikava
 */
public class CreditCardTest {

    private Card creditCard;
    private final String CARDHOLDER_NAME = "Anastasiya Melnikava";
    private final BigDecimal TEST_NUMBER = new BigDecimal("100.10");

    @Before
    public void setUp() {
        creditCard = new CreditCard(CARDHOLDER_NAME, TEST_NUMBER);
    }

    @Test
    public void testWithdrawFromCardBalance() {
        BigDecimal withdraw = new BigDecimal("100.00");
        creditCard.withdrawFromCardBalance(withdraw);
        BigDecimal expected = new BigDecimal("0.10");
        Assert.assertEquals(0, expected.compareTo(creditCard.getCardBalance()));
    }

    @Test
    public void testWithdrawFromCardBalanceIfAmountLessThanBalance() {
        BigDecimal withdraw = new BigDecimal("200.00");
        creditCard.withdrawFromCardBalance(withdraw);
        BigDecimal expected = new BigDecimal("-99.90");
        Assert.assertEquals(0, expected.compareTo(creditCard.getCardBalance()));
    }
}
