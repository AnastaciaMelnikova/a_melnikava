package com.amelnikava.task01;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Class Debit Card Test
 *
 * @author Anastasiya_Melnikava
 */

public class DebitCardTest {

    private Card debitCard;
    private final String CARDHOLDER_NAME = "Anastasiya Melnikava";
    private final BigDecimal TEST_NUMBER = new BigDecimal("100.10");

    @Before
    public void setUp() {
        debitCard = new DebitCard(CARDHOLDER_NAME, TEST_NUMBER);
    }

    @Test
    public void testWithdrawFromCardBalance() {
        BigDecimal withdraw = new BigDecimal("100.00");
        debitCard.withdrawFromCardBalance(withdraw);
        BigDecimal expected = new BigDecimal("0.10");
        Assert.assertEquals(0, expected.compareTo(debitCard.getCardBalance()));
    }

    @Test(expected = NotEnoughMoneyException.class)
    public void testWithdrawFromCardBalanceIfAmountLessThanBalance() {
        BigDecimal withdraw = new BigDecimal("200.00");
        debitCard.withdrawFromCardBalance(withdraw);
        throw new NotEnoughMoneyException("Not enough money");
    }
}