package com.amelnikava.task01;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Class Card Test
 *
 * @author Anastasiya_Melnikava
 */
public class CardTest {

    private Card card;
    private final String CARDHOLDER_NAME = "Anastasiya Melnikava";
    private final BigDecimal TEST_NUMBER = new BigDecimal("500.5646711");

    @Before
    public void setUp() {
        card = new Card(CARDHOLDER_NAME, TEST_NUMBER);
    }

    @Test
    public void testGetCardBalanceSetToScale() {
        BigDecimal expectedAmount = new BigDecimal("500.56");
        Assert.assertEquals(0, expectedAmount.compareTo(card.getCardBalance()));
    }

    @Test
    public void testAddToCardBalance() {
        BigDecimal addition = new BigDecimal("50.59");
        card.addToCardBalance(addition);
        BigDecimal expected = new BigDecimal("551.15");
        Assert.assertEquals(0, expected.compareTo(card.getCardBalance()));
    }

    @Test
    public void testWithdrawFromCardBalance() {
        BigDecimal withdraw = new BigDecimal("100.1548");
        card.withdrawFromCardBalance(withdraw);
        BigDecimal expected = new BigDecimal("400.41");
        Assert.assertEquals(0, expected.compareTo(card.getCardBalance()));
    }

    @Test
    public void testGetCardBalanceDifferentCurrency() {
        double exchangeRate = 2.042;
        double expected = 245.13;
        Assert.assertEquals(expected, card.getCardBalanceDifferentCurrency(exchangeRate), 0);
    }
}
