package com.amelnikava.task01;

import java.math.BigDecimal;

/**
 * Task 1 Class is Atm operation.
 *
 * @author Anastasiya_Melnikava
 */
public class Atm {

    private Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public void addCardBalance(String money) {
        card.addToCardBalance(new BigDecimal(money));
    }

    public void withdrawFromCardBalance(String money) {
        card.withdrawFromCardBalance(new BigDecimal(money));
    }
}
