package com.amelnikava.task01;

/**
 * Task 1 Class is described exception.
 *
 * @author Anastasiya_Melnikava
 */
public class NotEnoughMoneyException extends IllegalArgumentException {

    public NotEnoughMoneyException(String message) {
        super(message);
    }

    public NotEnoughMoneyException() {
        super();
    }
}
