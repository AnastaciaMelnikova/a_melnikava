package com.amelnikava.task01;

import java.math.BigDecimal;

/**
 * Task 1 Class is described debit card.
 *
 * @author Anastasiya_Melnikava
 */
public class DebitCard extends Card {

    private final String EXCEPTION_MESSAGE = "Not enough money";

    public DebitCard(String cardholderName) {
        super(cardholderName);
    }

    public DebitCard(String cardholderName, BigDecimal accountBalance) {
        super(cardholderName, accountBalance);
    }

    /**
     * Method for withdraw amount from balance.
     *
     * @param withdraw number to subtract
     * @throws NotEnoughMoneyException if there is not enough money, then an exception
     *                                 is caught and the amount is not removed from the balance.
     */
    @Override
    public void withdrawFromCardBalance(BigDecimal withdraw) {
        BigDecimal newBalance = super.getAccountBalance().subtract(withdraw);
        try {
            if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
                throw new NotEnoughMoneyException(EXCEPTION_MESSAGE);
            } else {
                super.setAccountBalance(newBalance);
            }
        } catch (NotEnoughMoneyException e) {
            System.out.println(e.getMessage());
        }
    }
}
