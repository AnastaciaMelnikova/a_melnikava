package com.amelnikava.task01;

import java.math.BigDecimal;

/**
 * Task 1 Class is described credit card.
 *
 * @author Anastasiya_Melnikava
 */
public class CreditCard extends Card {

    public CreditCard(String cardholderName) {
        super(cardholderName);
    }

    public CreditCard(String cardholderName, BigDecimal accountBalance) {
        super(cardholderName, accountBalance);
    }
}
