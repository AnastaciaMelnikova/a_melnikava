package com.amelnikava.task02;

/**
 * Task 2 Interface for sorting.
 *
 * @author Anastasiya_Melnikava
 */
public interface SortStrategy {
    /**
     * Method for sorting the array.
     *
     * @param array the array entered by user.
     */
    void sort(int[] array);
}
