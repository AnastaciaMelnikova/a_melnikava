package com.amelnikava.task02;

/**
 * Task 2 Class is described bubble sort.
 *
 * @author Anastasiya_Melnikava
 */
public class BubbleSort implements SortStrategy {

    @Override
    public void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
