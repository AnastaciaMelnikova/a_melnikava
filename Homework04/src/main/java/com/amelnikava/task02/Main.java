package com.amelnikava.task02;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Task 2 application main class.
 *
 * @author Anastasiya_Melnikava
 */
public class Main {

    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int RANDOM_SIZE = 100;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a positive number for array size:");
        int sizeArray = getNumber(scanner);

        int[] array = new int[sizeArray];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.round((Math.random() * RANDOM_SIZE));
            System.out.print(array[i] + " ");
        }

        System.out.println("\nChoose type of sorting:");
        System.out.println("1. Bubble sort");
        System.out.println("2. Selection sort");
        int sort = getChoose(scanner);
        scanner.close();

        SortingContext context = new SortingContext(getStrategy(sort));
        context.execute(array);

        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    /**
     * Method for get need strategy sorting.
     *
     * @param sort number received from user.
     * @return selected need sort object.
     */
    private static SortStrategy getStrategy(int sort) {
        SortStrategy sortStrategy = null;
        switch (sort) {
            case 1:
                sortStrategy = new BubbleSort();
                break;
            case 2:
                sortStrategy = new SelectionSort();
                break;
        }
        return sortStrategy;
    }

    /**
     * Method for get correct choose from user.
     *
     * @param scanner object for receiving data from the user.
     * @return correct choose from user
     */
    private static int getChoose(Scanner scanner) {
        int selectedNumber;
        do {
            try {
                selectedNumber = scanner.nextInt();
                if (selectedNumber < ONE || selectedNumber > TWO) {
                    System.out.println("You have not entered a need number. Try again.");
                } else {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("You have entered an invalid value. Try again.");
                scanner.nextLine();
            }
        } while (true);
        return selectedNumber;
    }

    /**
     * Method for get number from user.
     *
     * @param scanner object for receiving data from the user.
     * @return number from user
     */
    private static int getNumber(Scanner scanner) {
        int number;
        do {
            try {
                number = scanner.nextInt();
                if (number <= 0) {
                    System.out.println("You have entered a number less than 0. Try again.");
                } else {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("You have entered an invalid value. Try again.");
                scanner.nextLine();
            }
        } while (true);
        return number;
    }
}
