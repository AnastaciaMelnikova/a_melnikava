package com.amelnikava.task02;

/**
 * Task 2 Class is described selection sort.
 *
 * @author Anastasiya_Melnikava
 */
public class SelectionSort implements SortStrategy {

    @Override
    public void sort(int[] array) {
        for (int min = 0; min < array.length - 1; min++) {
            int least = min;
            for (int j = min + 1; j < array.length; j++) {
                if (array[j] < array[least])
                    least = j;
            }
            int temp = array[min];
            array[min] = array[least];
            array[least] = temp;
        }
    }
}
