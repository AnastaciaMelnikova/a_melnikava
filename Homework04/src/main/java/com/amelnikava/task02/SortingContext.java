package com.amelnikava.task02;

/**
 * Task 2 Class.
 *
 * @author Anastasiya_Melnikava
 */
public class SortingContext {

    private SortStrategy strategy;

    public SortingContext(SortStrategy strategy) {
        this.strategy = strategy;
    }

    public void execute(int[] array) {
        strategy.sort(array);
    }
}
