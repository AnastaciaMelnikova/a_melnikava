package com.amelnikava;

import java.util.*;

import static java.util.stream.Collectors.toMap;

/**
 * Application main class.
 *
 * @author Anastasiya_Melnikava
 */
public class Main {

    private static final String REGEX = "[^a-zA-Z]+";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Write the text:");
        String text = scanner.nextLine();

        List<String> words = Arrays.asList(text.split(REGEX));

        Map<String, Integer> map = words.stream().collect(toMap(String::toLowerCase, s -> 1, Integer::sum));
        Set<String> sortedKeys = new TreeSet<>(map.keySet());

        String firstLetter = sortedKeys.toString().toUpperCase().substring(0, 1);
        for (String key : sortedKeys) {
            if (!key.toUpperCase().substring(0, 1).equals(firstLetter)) {
                firstLetter = key.toUpperCase().substring(0, 1);
                System.out.print(firstLetter + ": ");
            }
            System.out.println(("\t" + key + ":" + map.get(key)));
        }
        scanner.close();
    }
}
