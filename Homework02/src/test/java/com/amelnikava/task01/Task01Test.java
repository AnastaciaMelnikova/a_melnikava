package com.amelnikava.task01;

import org.junit.Assert;
import org.junit.Test;

/**
 * Task 1 Test
 *
 * @author Anastasiya_Melnikava
 */
public class Task01Test {

    @Test
    public void testGetResult() {
        double expected = 29.28344162960579;
        int a = 3;
        int p = 2;
        double m1 = 3.5;
        double m2 = 5.6;
        double result = Task01.getResult(a, p, m1, m2);
        Assert.assertEquals(expected, result, 0.0);
    }

    @Test
    public void testParseInt() {
        int expected = 2;
        int actual = Task01.parseInt("2");
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseIntWithIncorrectArgument() {
        int expected = 2;
        int actual = Task01.parseInt("2.5");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testParseDouble() {
        double expected = 3.5;
        double actual = Task01.parseDouble("3.5");
        Assert.assertEquals(expected, actual, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseDoubleWithIncorrectArgument() {
        double expected = 3.5;
        double actual = Task01.parseDouble("three");
        Assert.assertEquals(expected, actual, 0);
    }
}