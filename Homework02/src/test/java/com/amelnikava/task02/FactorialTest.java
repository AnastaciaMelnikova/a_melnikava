package com.amelnikava.task02;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Task 2 Factorial Test
 *
 * @author Anastasiya_Melnikava
 */
public class FactorialTest {

    private Factorial factorial;
    private final int TEST_NUMBER = 5;
    private final long EXPECTED_RESULT = 120;

    @Before
    public void setUp() {
        factorial = new Factorial();
    }

    @Test
    public void testGetResultWithLoopWhile() {
        factorial.getResultWithLoopWhile(TEST_NUMBER);
        Assert.assertEquals(EXPECTED_RESULT, factorial.getResult());
    }

    @Test
    public void testGetResultWithLoopDoWhile() {
        factorial.getResultWithLoopDoWhile(TEST_NUMBER);
        Assert.assertEquals(EXPECTED_RESULT, factorial.getResult());
    }

    @Test
    public void testGetResultWithLoopFor() {
        factorial.getResultWithLoopFor(TEST_NUMBER);
        Assert.assertEquals(EXPECTED_RESULT, factorial.getResult());
    }
}