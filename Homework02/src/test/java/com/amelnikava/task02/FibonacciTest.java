package com.amelnikava.task02;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Task 2 Fibonacci Test
 *
 * @author Anastasiya_Melnikava
 */
public class FibonacciTest {

    private Fibonacci fibonacci;
    private final int TEST_NUMBER = 7;
    private final int[] EXPECTED_RESULT = {0, 1, 1, 2, 3, 5, 8};

    @Before
    public void setUp() {
        fibonacci = new Fibonacci(TEST_NUMBER);
    }

    @Test
    public void testGetResultWithLoopWhile() {
        fibonacci.getResultWithLoopDoWhile(TEST_NUMBER);
        Assert.assertArrayEquals(EXPECTED_RESULT, fibonacci.getFibonacci());
    }

    @Test
    public void testGetResultWithLoopDoWhile() {
        fibonacci.getResultWithLoopDoWhile(TEST_NUMBER);
        Assert.assertArrayEquals(EXPECTED_RESULT, fibonacci.getFibonacci());
    }

    @Test
    public void testGetResultWithLoopFor() {
        fibonacci.getResultWithLoopFor(TEST_NUMBER);
        Assert.assertArrayEquals(EXPECTED_RESULT, fibonacci.getFibonacci());
    }
}