package com.amelnikava.task02;

/**
 * Factorial class inherits an interface for implementing loop methods.
 *
 * @author Anastasiya_Melnikava
 */
public class Factorial implements LoopType {

    private long result = 1L;

    public long getResult() {
        return result;
    }

    /**
     * Method for get the result using a loop While.
     *
     * @param countOfNumbers number entered by user
     */
    @Override
    public void getResultWithLoopWhile(int countOfNumbers) {
        int i = 1;
        while (i <= countOfNumbers) {
            result = result * i;
            i++;
        }
    }

    /**
     * Method for get the result using a loop Do-While.
     *
     * @param countOfNumbers number entered by user
     */
    @Override
    public void getResultWithLoopDoWhile(int countOfNumbers) {
        int i = 1;
        do {
            result = result * i;
            i++;
        } while (i <= countOfNumbers);
    }

    /**
     * Method for get the result using a loop For.
     *
     * @param countOfNumbers number entered by user
     */
    @Override
    public void getResultWithLoopFor(int countOfNumbers) {
        for (int i = 1; i <= countOfNumbers; i++) {
            result = result * i;
        }
    }

    /**
     * Method to output the result to the console.
     */
    @Override
    public void printResult() {
        System.out.println("Result factorial: " + result);
    }
}
