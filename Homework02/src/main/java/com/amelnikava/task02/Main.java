package com.amelnikava.task02;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Task 2 application main class.
 *
 * @author Anastasiya_Melnikava
 */
public class Main {

    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose type of algorithm:");
        System.out.println("1. Calculation of a series of Fibonacci numbers");
        System.out.println("2. Factorial calculation");
        int algorithm = getChoose(scanner, TWO);

        System.out.println("Choose type of cycles:");
        System.out.println("1. Loop While");
        System.out.println("2. Loop Do-While");
        System.out.println("3. Loop For");
        int loopType = getChoose(scanner, THREE);

        System.out.println("Enter a positive number:");
        int number = getNumber(scanner);
        scanner.close();

        LoopType object;

        if (algorithm == ONE) {
            object = new Fibonacci(number);
        } else {
            object = new Factorial();
        }

        if (loopType == ONE) {
            object.getResultWithLoopWhile(number);
        } else if (loopType == TWO) {
            object.getResultWithLoopDoWhile(number);
        } else {
            object.getResultWithLoopFor(number);
        }
        object.printResult();
    }

    /**
     * Method for get number from user.
     *
     * @param scanner object for receiving data from the user.
     * @return number from user
     */
    private static int getNumber(Scanner scanner) {
        int number;
        do {
            try {
                number = scanner.nextInt();
                if (number <= 0) {
                    System.out.println("You have entered a number less than 0. Try again.");
                } else {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("You have entered an invalid value. Try again.");
                scanner.nextLine();
            }
        } while (true);
        return number;
    }

    /**
     * Method for get correct choose from user.
     *
     * @param scanner object for receiving data from the user.
     * @return correct choose from user
     */
    private static int getChoose(Scanner scanner, int value) {
        int selectedNumber;
        do {
            try {
                selectedNumber = scanner.nextInt();
                if (selectedNumber < ONE || selectedNumber > value) {
                    System.out.println("You have not entered a need number. Try again.");
                } else {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("You have entered an invalid value. Try again.");
                scanner.nextLine();
            }
        } while (true);
        return selectedNumber;
    }
}
