package com.amelnikava.task02;

import java.util.Arrays;

/**
 * Fibonacci class inherits an interface for implementing loop methods.
 *
 * @author Anastasiya_Melnikava
 */
public class Fibonacci implements LoopType {

    private final int FIRST_NUMBER = 0;
    private final int SECOND_NUMBER = 1;
    private int[] fibonacci;

    public Fibonacci(int countOfNumbers) {
        fibonacci = new int[countOfNumbers];
        fibonacci[FIRST_NUMBER] = FIRST_NUMBER;
        if (countOfNumbers >= 2) {
            fibonacci[SECOND_NUMBER] = SECOND_NUMBER;
        }
    }

    public int[] getFibonacci() {
        return fibonacci;
    }

    /**
     * Method for get the result using a loop While.
     *
     * @param countOfNumbers number entered by user
     */
    @Override
    public void getResultWithLoopWhile(int countOfNumbers) {
        int i = 2;
        while (i < countOfNumbers) {
            fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
            i++;
        }
    }

    /**
     * Method for get the result using a loop Do-While.
     *
     * @param countOfNumbers number entered by user
     */
    @Override
    public void getResultWithLoopDoWhile(int countOfNumbers) {
        int i = 2;
        if (countOfNumbers <= i) {
            return;
        }
        do {
            fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
            i++;
        } while (i < countOfNumbers);
    }

    /**
     * Method for get the result using a loop For.
     *
     * @param countOfNumbers number entered by user
     */
    @Override
    public void getResultWithLoopFor(int countOfNumbers) {
        for (int i = 2; i < countOfNumbers; i++) {
            fibonacci[i] = fibonacci[i - 2] + fibonacci[i - 1];
        }
    }

    /**
     * Method to output the result to the console.
     */
    @Override
    public void printResult() {
        System.out.println("Fibonacci: " + Arrays.toString(fibonacci));
    }
}
