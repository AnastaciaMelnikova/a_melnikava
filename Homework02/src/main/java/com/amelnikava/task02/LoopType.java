package com.amelnikava.task02;

/**
 * Interface for loop types.
 *
 * @author Anastasiya_Melnikava
 */
public interface LoopType {
    /**
     * Method for calculating the result using a loop While.
     *
     * @param countOfNumbers number entered by user
     */
    void getResultWithLoopWhile(int countOfNumbers);

    /**
     * Method for calculating the result using a loop Do-While.
     *
     * @param countOfNumbers number entered by user
     */
    void getResultWithLoopDoWhile(int countOfNumbers);

    /**
     * Method for calculating the result using a loop For.
     *
     * @param countOfNumbers number entered by user
     */
    void getResultWithLoopFor(int countOfNumbers);

    /**
     * Method to output the result to the console.
     */
    void printResult();
}
