package com.amelnikava.task01;

/**
 * Task 1 application class.
 *
 * @author Anastasiya_Melnikava
 */
public class Task01 {

    private static final double PI = Math.PI;
    private static final int FIRST_ARGUMENT = 0;
    private static final int SECOND_ARGUMENT = 1;
    private static final int THIRD_ARGUMENT = 2;
    private static final int FOURTH_ARGUMENT = 3;
    private static final int COUNT_ARGUMENTS = 4;

    /**
     * Application start point.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {

        if (args == null || args.length < COUNT_ARGUMENTS) {
            System.out.println("Wrong number of arguments");
            return;
        }

        int a = parseInt(args[FIRST_ARGUMENT]);
        int p = parseInt(args[SECOND_ARGUMENT]);
        double m1 = parseDouble(args[THIRD_ARGUMENT]);
        double m2 = parseDouble(args[FOURTH_ARGUMENT]);

        System.out.println(getResult(a, p, m1, m2));
    }

    /**
     * Method for parse argument to int.
     *
     * @param arg actual argument
     * @return parsed argument
     * @throws NumberFormatException thrown to indicate that the application has attempted to convert
     *                               a string to int, but that the string does not have the appropriate format.
     */
    static int parseInt(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        }
    }

    /**
     * Method for parse argument to double.
     *
     * @param arg actual argument
     * @return parsed argument
     * @throws NumberFormatException thrown to indicate that the application has attempted to convert
     *                               a string to double, but that the string does not have the appropriate format.
     */
    static double parseDouble(String arg) {
        try {
            return Double.parseDouble(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Incorrect argument " + arg, e);
        }
    }

    /**
     * Method for get result.
     *
     * @param a  first argument
     * @param p  second argument
     * @param m1 third argument
     * @param m2 fourth argument
     * @return result
     */
    static double getResult(int a, int p, double m1, double m2) {
        return 4 * Math.pow(PI, 2) * (Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2)));
    }
}
