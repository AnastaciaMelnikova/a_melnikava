<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<title>Product Page</title>
</head>

<body>
<h1 align="center">Hello ${name}!</h1>
<div align="center">
        <c:if test="${userList != null}">
            <h5>You have already choosen: </h5>
            <ol>
            <c:forEach items="${userList}" var="item">
                <li>${item}$</li>
            </c:forEach>
        </c:if>
        <c:if test="${userList == null}">
            <h3>Make your order: </h3>
        </c:if>
     </ol>

    <form method="post" action="/order">
        <input type="hidden" value="${name}" name="name">
        <select name="good" size="5">
            <%--@elvariable id="products" type="java.util.Map"--%>
            <c:forEach var="product" items="${products}">
                <option>${product.getKey()} ${product.getValue()}</option>
            </c:forEach>
        </select><br/>
        <input type="submit" value="Add Item" name="add">
        <input type="submit" value="Submit">
    </form>
</div>
</body>
</html>