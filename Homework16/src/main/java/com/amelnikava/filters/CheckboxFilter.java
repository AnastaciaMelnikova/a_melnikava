package com.amelnikava.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class CheckboxFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        String agreement = request.getParameter("agreement");

        session.setAttribute("agreement", agreement);
        PrintWriter out = response.getWriter();

        if (agreement == null) {
            out.println("<html>");
            out.println("<head><title>Oops!</title></head>");
            out.println("<body>"
                    + "<h1 align=\"center\">Oops!</h1>"
                    + "<p>You shouldn't be here</p>"
                    + "<p>Please, agree with the terms of service first.</p>"
                    + "<a href=\"/\">Start page</a>"
                    + "</body>");
            out.println("</html>");
            out.close();
        } else {
            chain.doFilter(req, response);
        }
    }

    @Override
    public void destroy() {

    }
}
