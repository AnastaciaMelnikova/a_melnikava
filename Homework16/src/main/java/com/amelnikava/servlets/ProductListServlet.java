package com.amelnikava.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Second servlet.
 *
 * @author Anastasiya_Melnikava
 */

public class ProductListServlet extends HttpServlet {

    private Map<String, Double> products;
    private final List<String> userList = new ArrayList<>();

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        final ServletContext servletContext = config.getServletContext();
        products = Collections.list(servletContext.getInitParameterNames())
                .stream()
                .collect(Collectors.toMap(name -> name, name -> Double.valueOf(servletContext.getInitParameter(name))));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        HttpSession session = req.getSession();
        String name = req.getParameter("name");
        Optional<String> addButton = Optional.ofNullable((String) session.getAttribute("add"));

        if (addButton.isPresent()) {
            session.setAttribute("add", addButton);
            doPost(req, resp);
        }
        session.setAttribute("name", name);
        session.setAttribute("products", products);
        req.getRequestDispatcher("product.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.getRequestDispatcher("product.jsp");
        Optional<String> good = Optional.of(req.getParameter("good"));
        HttpSession session = req.getSession();

        userList.add(good.get());
        session.setAttribute("userList", userList);
        req.getRequestDispatcher("/product.jsp").forward(req, resp);
    }
}
