package com.amelnikava.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Third servlet.
 *
 * @author Anastasiya_Melnikava
 */
public class OrderServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        String name = req.getParameter("name");

        List<String> order = (List<String>) session.getAttribute("userList");

        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<head><title>Order Servlet</title></head>");
        out.println("<body>"
                + "<h1 align=\"center\">Dear " + name + ", your order:</h1>"
                + "<div align=\"center\">"
                + "<p>" + Arrays.toString(order.toArray()) + "</p>"
                + "<h4>Total: " + getTotalSum(order) + "$</h4>"
                + "</div>"
                + "</body>");
        out.println("</html>");
        out.close();
    }


    /**
     * Method for creating a product list.
     *
     * @param order array of products selected by the user
     * @return list of products in the form of map
     */
    private Map<String, Double> getProductMap(List<String> order) {
        Map<String, Double> productList = new HashMap<>();
        if (order != null) {
            for (String item : order) {
                String[] keyValue = item.split(" ");
                productList.put(keyValue[0], Double.valueOf(keyValue[1]));
            }
        }
        return productList;
    }

    /**
     * Method for calculating the total amount.
     *
     * @param order products selected by the user
     * @return amount
     */
    private double getTotalSum(List<String> order) {
        return order.stream()
                .map(value -> value.replaceAll("\\D+\\s\\b", " "))
                .map(Double::valueOf)
                .reduce(0d, Double::sum);
    }
}
